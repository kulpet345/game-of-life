import sys

from . import lib


def input_from_file(name_of_file):
    fin = open(name_of_file, 'r')
    cnt_operations, n, m = [int(x) for x in fin.readline().split()]
    start_field = []
    for s in fin.readlines():
        start_field.append(list(s.strip()))
    return cnt_operations, n, m, start_field


def input_from_stdin():
    cnt_operations, n, m = [int(x) for x in input().split()]
    start_field = []
    for s in sys.stdin.readlines():
        start_field.append(list(s.strip()))
    return cnt_operations, n, m, start_field


def output_in_file(name_of_file, res):
    fout = open(name_of_file, 'w')
    for i in range(len(res)):
        for j in range(len(res[i])):
            fout.write(res[i][j])
        fout.write('\n')


def output_in_stdout(res):
    for i in range(len(res)):
        for j in range(len(res[i])):
            print(res[i][j], end='')
        print('')

