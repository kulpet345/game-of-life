from setuptools import setup, find_packages

def parse_requirements():
    with open('requirements.txt') as f:
        return f.read().splitlines()


setup(
    name="gameoflife",
    version='1.0',
    author=['Petr Kulagin'],
    packages=find_packages(),
    long_description='game of life',
    requirements=parse_requirements()
)
